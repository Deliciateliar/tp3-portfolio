/* eslint-disable jsx-a11y/iframe-has-title */
import "./style.css";

const Contact = () => {
    return (
        <div className="contact">
            <h1>Me contacter</h1>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9955.204081732574!2d30.04495194114536!3d51.40671070898116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472a7daa5b69c469%3A0x32d80be9b6bf089a!2sPryp&#39;yat&#39;%2C%20Kyiv%20Oblast%2C%20Ukraine!5e0!3m2!1sen!2sca!4v1634503958339!5m2!1sen!2sca" 
                frame-border="0"
                allowFullscreen=""
                aria-hidden="false"
                tabIndex="0">
            </iframe>
            <h4>Téléphone</h4>
            <p>555-555-5555</p>

            <h4>Courriel</h4>
            <p>pleaujess@gmail.com</p>

            <h4>Adresse</h4>
            <p>Pryp'yat' Kyiv Oblast, Ukraine</p>
            
        </div>
    )
}

export default Contact
