import "./style.css";

const Skills = () => {
    return (
        <div className="skills">
            <h1>Compétences</h1>
            <div className="content">
                <div>
                    <strong>Langages</strong>
                  
                    <li>CSS3</li>
                    <li>JavaScript</li>
                    <li>HTML5</li>
                </div>
                <div>
                    <strong>Librairies</strong>
                    <li>Bootstrap</li>
                    <li>React.js</li>
                </div>
                
                
            </div>
            
        </div>
    )
}

export default Skills
