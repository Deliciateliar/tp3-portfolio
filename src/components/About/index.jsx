import "./style.css";

const About = () => {
    return (
        <div className="about">
            <h1>À Propos</h1>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante arcu, ornare vitae sem rutrum, finibus tempus dui. Nam dui elit, volutpat in libero ac, bibendum efficitur nisl. Cras rhoncus viverra blandit. In consequat mattis posuere. Etiam iaculis dolor at tellus malesuada sodales. Donec ultricies facilisis turpis non aliquet. Pellentesque malesuada venenatis molestie.
            Aenean sit amet vulputate enim. Curabitur laoreet, ipsum eget ullamcorper sollicitudin, nibh enim laoreet odio, at vehicula nisl leo vitae mi. Vestibulum lorem nibh, varius quis placerat vitae, iaculis non est. Mauris viverra rutrum congue. Quisque fringilla venenatis mauris mollis finibus. Fusce velit massa, facilisis quis metus rhoncus, venenatis pharetra enim. Suspendisse posuere ullamcorper bibendum. Ut porta nisl et blandit dapibus. Etiam lobortis a est id mollis. Morbi iaculis mauris in finibus congue. Pellentesque cursus dapibus luctus. Maecenas volutpat cursus nunc a mollis. Proin ornare, neque id feugiat iaculis, velit nibh tincidunt sapien, eget rhoncus felis mauris non nunc. Donec tristique eleifend auctor. Mauris vitae nisi ac erat hendrerit tincidunt. Mauris facilisis vel lectus vitae condimentum.
            </p>
            <br/>
            <p>
            Donec pharetra consequat magna, ut consectetur arcu faucibus quis. Nunc luctus justo ac cursus vehicula. Nunc sit amet auctor eros. Maecenas at nisi et massa congue volutpat vel non libero. Vestibulum non consectetur lectus, eget condimentum sapien. Praesent eget diam sapien. Etiam mollis elit metus, nec efficitur erat ultrices sit amet. Suspendisse consequat risus nunc, ut commodo orci suscipit ut. Mauris iaculis ultricies odio, et condimentum lectus efficitur eget. Vivamus vehicula efficitur dapibus. Suspendisse viverra nulla leo, sit amet scelerisque magna blandit ac. Cras tristique imperdiet erat. In ornare nisl et quam eleifend, id laoreet eros porttitor. Phasellus nec fermentum metus.
            Praesent eget iaculis sapien, sed blandit mauris. Vivamus viverra mattis faucibus. Donec bibendum aliquam felis ut aliquam. Quisque in velit urna. Sed ullamcorper neque id felis convallis, nec elementum lacus cursus. Nulla facilisi. Nullam tincidunt bibendum nunc, vel laoreet sem commodo in.
            </p>

        </div>
    )
}

export default About
