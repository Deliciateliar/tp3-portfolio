import {Link, useLocation} from "react-router-dom";
import "./style.css";

const Home = () => {
    return (
        <div className="home">
            <div className="title">
                <h1>
                    
                    <p>Jessica Pleau</p>
                    <p>Développeuse Web</p>
                </h1>
                <Link to ="about">
                    <button>Plus d'informations</button>
                </Link>
            </div>
            <div className="person">
                <img src ={`${process.env.PUBLIC_URL}/jess-logo.png`} alt="person"/>

            </div>

        </div>
    )
}

export default Home
