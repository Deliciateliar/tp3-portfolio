import "./style.css";
import {useState, useEffect} from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
    const[open, setOpen] = useState(false);
    const[screenWidth, setScreenWidth] = useState(0);
    const trackScreenWidth = () => {
        const width = window.innerWidth;
        setScreenWidth(width);
        if(width>800){
            setOpen(true);
        }
    };
    useEffect(() =>{
        trackScreenWidth();
        window.addEventListener("resize", trackScreenWidth);
        return ()=> window.removeEventListener("resize", trackScreenWidth);
    }, []);
  return (
    <nav className="navbar">
      <div className="nav-wrapper">
        <div className="logo">
          <Link to="/">
            <img src={`${process.env.PUBLIC_URL}/logo.png`} alt="brand" />
          </Link>
        </div>
        <div className="list-wrapper">
          <img
            src={`${process.env.PUBLIC_URL}/menu-bars.png`}
            alt="menu-bars"
            style={{opacity:!open ? 1 : 0}}
            onClick={()=> {
                setOpen(!open);
            }}
          />
          <img
            src={`${process.env.PUBLIC_URL}/menu-cross.png`}
            alt="menu-cross"
            style={{opacity: open ? 1 : 0}}
            onClick={()=> {
                setOpen(!open);
            }}
          />
          <ul style={{left: open ? "0" : "-100vw"}}>
          <li>
              <Link to="/">Accueil</Link>
            </li>
           
            <li>
              <Link to="/about">À Propos</Link>
            </li>

            <li>
              <Link to="/skills">Compétences</Link>
            </li>
            <li>
              <Link to="/works">Expérience</Link>
            </li>
            <li>
              <Link to="/contact">Contact</Link>
            </li>
           
           
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
