import "./style.css";
const Footer = () => {
  return (
    <footer>
        <a href="https://www.facebook.com">
        <img
          src={`${process.env.PUBLIC_URL}/facebook.png`}
          alt="facebook-icon"
        />
      </a>
      <a href="https://gitlab.com/Deliciateliar">
        <img
          src={`${process.env.PUBLIC_URL}/gitlab.png`}
          alt="gitlab-icon"
        />
      </a>
      <a href="https://www.linkedin.com/home">
        <img
          src={`${process.env.PUBLIC_URL}/linkedin.png`}
          alt="linkedin-icon"
        />
      </a>
    
      
    </footer>
  );
};

export default Footer;
