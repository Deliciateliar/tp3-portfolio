import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from './components/Home';
import Footer from './components/Footer'; 
import About from './components/About';
import NavBar from './components/Navbar';
import Skills from './components/Skills';
import Works from './components/Works';
import Contact from './components/Contact';
import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import HttpApi from 'i18next-http-backend';
import "./index.css";
import "bootstrap/dist/js/bootstrap.js"
import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css"

i18n
.use(initReactI18next)
.use(LanguageDetector)
.use(HttpApi)
.init({
  supportedLngs:['en','fr'],
  fallbackLng: "en",
  detection:{
    order: ['cookie', 'htmlTag', 'localStorage','path', 'subdomain'],
    caches:['cookie'],
  },
  backend:{
    loadPath: '/assets/locales/{{lng}}/translation.json',

  },
  react:{useSuspense: false }
});


function App() {
  const { t } = useTranslation();
  return (
    <Router>
      <div className="app">
        <NavBar/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/skills" component={Skills}/>
          <Route exact path="/works" component={Works}/>
          <Route exact path="/contact" component={Contact}/>
        </Switch>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;
